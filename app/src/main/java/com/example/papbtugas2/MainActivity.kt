package com.example.papbtugas2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnSubmit = findViewById<Button>(R.id.btnSubmit)
        val edUsername = findViewById<EditText>(R.id.edUsername)
        val edPassword = findViewById<EditText>(R.id.edPassword)

        btnSubmit.setOnClickListener{
            val Username = edUsername.text.toString()
            val Password = edPassword.text.toString()

            Intent(this, ScondActivity::class.java).also {
                it.putExtra("Extra_Username", Username)
                it.putExtra("Extra_Password", Password)
                startActivity(it)
            }
        }
    }
}
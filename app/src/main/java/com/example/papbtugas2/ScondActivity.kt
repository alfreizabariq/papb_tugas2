package com.example.papbtugas2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ScondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scond2)

        val tvHasil = findViewById<TextView>(R.id.tvHasil)

        val Username  = intent.getStringExtra("Extra_Username")
        val Password  = intent.getStringExtra("Extra_Password")

        val hasilData = "Username anda adalah $Username \n" +
                        "Password anda adalah $Password \n"
        tvHasil.text = hasilData
    }
}